﻿using System;
using System.Linq;
using System.Threading;
using System.Text;
//using Crestron;
using Crestron.SimplSharp;// For Basic SIMPL# Classes
using Crestron.SimplSharp.CrestronSockets;
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp.Net;
using Crestron.SimplSharp.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AdderDDX
{
    

    public class DDXClient
    {

        /// <summary>
        /// SIMPL+ can only execute the default constructor. If you have variables that require initialization, please
        /// use an Initialize method
        /// </summary>
        ///     
        public string URL;
        public string Path;
        public static string AuthToken;
        public string Username;// = "admin";
        public string Password;// = "password";
        private string _Response;
         
        private MyHttpClient MyClient = new MyHttpClient();
        LoginInfo loginInfo = new LoginInfo();        

        private class MyHttpClient : HttpClient
        {
            public MyHttpClient()
            {
                this.TimeoutEnabled = true;
                this.Timeout = 1;
                this.KeepAlive = false;
                //this.UserName = "admin";
                //this.Password = "password";
            }
        }
        public delegate void _HTTPClientStringCallback(	string userobj,	HTTP_CALLBACK_ERROR error);
        private class MyHttpClientRequest : HttpClientRequest
        {
            public MyHttpClientRequest(string _URL, string _Path, string _ContentString)
            {
                string _uri = _URL + _Path;
                string _AuthString = "bearer " + DDXClient.AuthToken;
                CrestronConsole.PrintLine(_uri);
                CrestronConsole.PrintLine(_AuthString);
                CrestronConsole.PrintLine("Content: {0}", _ContentString);
                this.Url.Parse(_uri);
                this.Header.SetHeaderValue("Content-Transfer-Encoding", "chunked");
                this.Header.AddHeader(new HttpHeader("accept", "application/json"));
                this.Header.AddHeader(new HttpHeader("expect", "*"));
                this.Header.AddHeader(new HttpHeader("Authorization", _AuthString));
                this.Header.AddHeader(new HttpHeader("Content-Type", "application/json"));
                this.ContentString = _ContentString;
            }
        }
        private class LogInHttpClientRequest : HttpClientRequest
        {
            public LogInHttpClientRequest(string _URL, string _Path, string _ContentString)
            {
                string _uri = _URL + _Path;
                CrestronConsole.PrintLine(_uri);
                CrestronConsole.PrintLine("Content: {0}", _ContentString);
                this.Url.Parse(_uri);
                this.Header.SetHeaderValue("Content-Transfer-Encoding", "chunked");
                this.Header.AddHeader(new HttpHeader("expect", "*"));
                this.Header.AddHeader(new HttpHeader("accept", "application/json"));
                this.Header.AddHeader(new HttpHeader("Content-Type", "application/json"));
                this.ContentString = _ContentString;
            }
        }
        private string ParseForToken()
        {
            string _sAuthToken;
            try
            {
                JObject jResponse = JObject.Parse(_Response.TrimStart(' '));
                //For debuging
                CrestronConsole.PrintLine("Jobject is: {0}\n", jResponse);
                var _token = jResponse["token"];
                _sAuthToken = _token.ToString().Trim('"');
                CrestronConsole.PrintLine("Token is: {0}\n", _sAuthToken);
                return _sAuthToken;
            }
            catch (JsonReaderException e)
            {
                CrestronConsole.PrintLine("Json Reader exception: {0}\n", e.Message);
            }
            catch (JsonSerializationException e)
            {
                CrestronConsole.PrintLine("Json Serial exception: {0}\n", e.Message);
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("General Exception in the Json part: {0}\n", e.Message);
            }
            return "";
        }

        public void SendGetToken()
        {
            try
            {
                loginInfo.username = Username;
                loginInfo.password = Password;
                string _ContentString = JsonConvert.SerializeObject(loginInfo);

                LogInHttpClientRequest LogIn = new LogInHttpClientRequest(URL, "auth/local", _ContentString);
                LogIn.RequestType=RequestType.Post;
                HttpClientResponse LogInResponse;
                
                LogInResponse=MyClient.Dispatch(LogIn);
                MyClient.Dispose();
                
                _Response = LogInResponse.ContentString;
                DDXClient.AuthToken = ParseForToken();
                CrestronConsole.Print(_Response);
            }
            catch (HttpException e)
            {
                CrestronConsole.PrintLine("HTTP Exception: {0}\n", e.Message);
            }
            catch (HttpHeaderException e)
            {
                CrestronConsole.PrintLine("Header Execption: {0}\n", e.Message);
            }
            catch (HttpRequestInvalidException e)
            {
                CrestronConsole.PrintLine("HTTP Request Exception: {0}\n", e.Message);
            }
        }
        public void SendConsoleSwitch(int _ConsoleId,int _ComputerId)
        {
            try
            {
                CrestronConsole.PrintLine("Token is: {0}", DDXClient.AuthToken);
                CrestronConsole.PrintLine("Switch Console: {0} to {1}", _ConsoleId, _ComputerId);

                string _AuthString = "bearer " + DDXClient.AuthToken;
                CrestronConsole.PrintLine("AuthString is: '{0}'", _AuthString);
                ConsoleInfo consoleInfo = new ConsoleInfo();
                consoleInfo.computerId = _ComputerId;
                consoleInfo.mode = "VIEWONLY";
                string _ContentString = JsonConvert.SerializeObject(consoleInfo);
                MyHttpClientRequest ConsoleSwitch = new MyHttpClientRequest(URL, "consoles/" + _ConsoleId + "/switch", _ContentString);
                ConsoleSwitch.RequestType = RequestType.Post;
                HttpClientResponse Response;
                Response = MyClient.Dispatch(ConsoleSwitch);
                MyClient.Dispose();
                _Response = Response.ContentString;
                CrestronConsole.Print(_Response);
            }
            catch (HttpException e)
            {
                CrestronConsole.PrintLine("HTTP Exception: {0}\n", e.Message);
                SendGetToken();
                SendConsoleSwitch(_ConsoleId, _ComputerId);
            }
            catch (HttpHeaderException e)
            {
                CrestronConsole.PrintLine("Header Execption: {0}\n", e.Message);
            }
            catch (HttpRequestInvalidException e)
            {
                CrestronConsole.PrintLine("HTTP Request Exception: {0}\n", e.Message);
            }
        }
        public void SendGetSystemInfo()
        {
            try
            {
                HttpClientRequest _GetSystemInfo = new HttpClientRequest();
                string _uri = URL + "system/systeminfo";
                string _AuthString = "bearer " + DDXClient.AuthToken;
                CrestronConsole.PrintLine("AuthString is: '{0}'", _AuthString);
                _GetSystemInfo.Url.Parse(_uri);
                _GetSystemInfo.Header.SetHeaderValue("Content-Transfer-Encoding", "chunked");
                _GetSystemInfo.Header.AddHeader(new HttpHeader("expect", "*"));
                _GetSystemInfo.Header.AddHeader(new HttpHeader("accept", "application/json"));
                _GetSystemInfo.Header.AddHeader(new HttpHeader("Authorization", _AuthString));
                _GetSystemInfo.RequestType = RequestType.Get;
                HttpClientResponse Response;
                
                Response = MyClient.Dispatch(_GetSystemInfo);
                MyClient.Dispose();
                _Response = Response.ContentString;

                CrestronConsole.Print(_Response);
            }
            catch (HttpException e)
            {
                CrestronConsole.PrintLine("HTTP Exception: {0}\n", e.Message);
            }
            catch (HttpHeaderException e)
            {
                CrestronConsole.PrintLine("Header Execption: {0}\n", e.Message);
            }
            catch (HttpRequestInvalidException e)
            {
                CrestronConsole.PrintLine("HTTP Request Exception: {0}\n", e.Message);
            }
        }
    }

    public class LoginInfo
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class ConsoleInfo
    {
        public int computerId { get; set; }
        public string mode { get; set; }
    }
}
